using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlGenerator {
    class RogerioMain {

        public static void main() {

            ExcelUtils.initXls();
            //string t1 = "GAA_TIPO_PLANO_FPF";
            //string t2 = "GAA_TIPO_PLANO_FPA"; 

            string t1 = "GAA_EXPLORACAO_VIDA";
            string t2 = null;
            string t1Hist = "GAA_HIST_EXPLORACAO_VIDA";
            string entity = "ExploracaoVida";
            string tipoFundo = "LPS_FORA_PT";

            doSql(t1, t1Hist, tipoFundo);

            doCode(t1, t2, entity, tipoFundo);

            ExcelUtils.closeXLS();

        }

        private static void doCode(string t1, string t2, string entity, string tipoFundo) {
            List<string[]> columnsT1 = SqlUtils.getTableColumns(t1);
            List<string[]> columnsT2 = SqlUtils.getTableColumns(t2);
            List<string> pksT1 = SqlUtils.getTablePrimaryKeys(t1);
            List<string> pksT2 = SqlUtils.getTablePrimaryKeys(t2);
            List<String> replaceablePKsT1 = ExcelUtils.getReplaceablePKs(t1);
            List<String> replaceablePKsT2 = ExcelUtils.getReplaceablePKs(t2);

            List<string[]> union = DbUtils.unionColumns(columnsT1, columnsT2);
            List<string> pks = DbUtils.unionPksColumns(pksT1, pksT2);

            FormUtils.generateEditFormUI(entity, union, pks, replaceablePKsT1);
            FormUtils.generateEditFormCode(t1, t2, entity, columnsT1, columnsT2, pksT1, pksT2, replaceablePKsT1, replaceablePKsT2);

        }

        private static string getHeader(string context) {
            string h = "/******************************************************************************************************\r\n";
            h += "*******************************************************************************************************\r\n";
            h+= context.ToUpper() + "\r\n";
            h += "*******************************************************************************************************\r\n";
            h += "******************************************************************************************************/\r\n\r\n\r\n\r\n";
            return h;
        }

        private static void doSql(string table, string histTable, String tipoFundo) {

            List<String> spVersao = ExcelUtils.getSPVersoes(table);
            List<string> pks = SqlUtils.getTablePrimaryKeys(table);
            List<String> replaceablePKs = ExcelUtils.getReplaceablePKs(table);
            List<String> descs = ExcelUtils.getDescription(table);
            List<string[]> columns = SqlUtils.getTableColumns(table);
            String description = descs.Count() == 0 ? "" : descs[0];
            String version = spVersao.Count() == 0 ? "" : spVersao[0];

            
            string content = "";
            try {
                string output = DbUtils.generateDetail(
                    table,
                    description,
                    version,
                    columns,
                    pks);
                content += getHeader("DETALHE SP") + output;
            } catch (Exception e) {
                Console.WriteLine("Erro: " + e.Message);
            }

            try {
                string output = DbUtils.generateHT(
                    table,
                    histTable,
                    description,
                    version,
                    columns,
                    pks, replaceablePKs);
                content += getHeader("UPDATE SP") + output;
            } catch (Exception e) {
                Console.WriteLine("Erro: " + e.Message);
            }


            try {
                string output = DbUtils.generateInserts(
                    table, tipoFundo,
                    description,
                    version,
                    columns,
                    pks, replaceablePKs);
                content += getHeader("INSERT SP") + output;
            } catch (Exception e) {
                Console.WriteLine("Erro: " + e.Message);
            }

            try {
                string output = DbUtils.generateUpdates(
                    table,
                    description,
                    version,
                    columns,
                    pks, replaceablePKs);
                content += getHeader("UPDATE SP") + output;
            } catch (Exception e) {
                Console.WriteLine("Erro: " + e.Message);
            }

            FileUtils.writeGeneratedFile("output.sql", content);
        }



    }
}
