using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlGenerator {
    class Utils {
        public static string formatDate(DateTime data) {
            if (data == null) {
                return "";
            }
            return ((DateTime)data).ToString("dd-MM-yyyy");
        }

    }
}
