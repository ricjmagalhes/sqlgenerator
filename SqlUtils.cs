using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace SqlGenerator {
    class SqlUtils {

        private static SqlConnection conn = null;

        public static string transfProcInArg(string name, string type, string size = null, string precision = null, string scale = null) {
            string sql = "\t@" + name + " " + type;
            if (size != null) {
                sql += "(" + size + ")";
            } else if (type.ToUpper().Equals("DECIMAL") && precision != null && scale != null) {
                sql += "(" + precision + ", " + scale + ")";
            }
            return sql;
        }

        public static SqlConnection getConnection() {
            if (conn == null) {
                string sdwConnectionString = @"Data Source=xxxxxx;Initial Catalog=xxxxxxx;Integrated Security=False;user id=USER_GAA; password=xxxxxxxxx; Application Name=GAA";
                conn = new SqlConnection(sdwConnectionString);
                conn.Open();
            }
            return conn;
        }

        public static void closeConnection() {
            if (conn != null) {
                conn.Close();
            }
        }

        public static List<string[]> getTableColumns(string table) {
            List<string[]> values = new List<string[]>();
            if (!string.IsNullOrEmpty(table)) {
                string query = @"select 
                            ColumnName = col.column_name, 
                            ColumnDataType = col.data_type,
                            ColumnSize = col.CHARACTER_MAXIMUM_LENGTH,
                            ColumnPrecision = numeric_precision,
                            ColumnScale = numeric_scale,
                            ColumnNullable = (case is_nullable when 'NO' then 0 else 1 end )
                            FROM information_schema.tables tbl
                            INNER JOIN information_schema.columns col 
                                ON col.table_name = tbl.table_name
                            where tbl.table_name = '" + table + "' order by ORDINAL_POSITION ASC";

                SqlCommand queryCommand = new SqlCommand(query, getConnection());
                SqlDataReader reader = queryCommand.ExecuteReader();

                while (reader.Read()) {
                    string tipo = reader.GetString(1);
                    string nome = reader.GetString(0);
                    string size = reader.GetValue(2).ToString();
                    string precision = reader.GetValue(3).ToString();
                    string scale = reader.GetValue(4).ToString();
                    string nullable = reader.GetValue(5).ToString();

                    size = string.IsNullOrEmpty(size) ? null : size;
                    precision = string.IsNullOrEmpty(precision) ? null : precision;
                    scale = string.IsNullOrEmpty(scale) ? null : scale;

                    values.Add(new string[] { nome, tipo, size, precision, scale, nullable });

                }
                reader.Close();
            }
            return values;
        }


        public static List<string> getTablePrimaryKeys(string table) {
            List<string> values = new List<string>();
            if (!string.IsNullOrEmpty(table)) {
                string query = @"SELECT u.COLUMN_NAME
	                        FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS AS c INNER JOIN
		                        INFORMATION_SCHEMA.KEY_COLUMN_USAGE AS u 
	                        ON c.CONSTRAINT_NAME = u.CONSTRAINT_NAME
	                        WHERE
		                        u.TABLE_NAME = c.TABLE_NAME
		                        and c.CONSTRAINT_TYPE = 'PRIMARY KEY'
		                        and u.TABLE_NAME = '" + table + "'";

                SqlCommand queryCommand = new SqlCommand(query, getConnection());
                SqlDataReader reader = queryCommand.ExecuteReader();

                while (reader.Read()) {
                    values.Add(reader.GetString(0));
                }
                reader.Close();
            }
            return values;
        }


        public static string getInParams(List<string[]> columns, bool update, List<String> replaceablePKs)
        {
            StringBuilder sql = new StringBuilder();

            int len = columns.Count();
            foreach (string[] column in columns)
            {
                --len;

                if (!column[0].ToUpper().Equals("DTREGISTO") 
                    && !column[0].ToUpper().Equals("CODUTILIZADORREGISTO")
                    && !column[0].ToUpper().Equals("CODUTILIZADORALTERACAO"))
                {
                    sql.Append(SqlUtils.transfProcInArg(column[0], column[1], column[2], column[3], column[4]));
                    if (len >= 0)
                    {
                        sql.Append(",");
                    }
                    sql.Append("\n");
                    if (update && replaceablePKs.Contains(column[0]))
                    {
                        sql.Append(SqlUtils.transfProcInArg("old" + column[0], column[1], column[2], column[3], column[4]));
                        if (len >= 0)
                        {
                            sql.Append(",");
                        }
                        sql.Append("\n");
                    }
                }

            }
            return sql.ToString();
        }


        public static string getInParamsHT(List<string[]> columns, List<string> pks, List<String> replacePks)
        {
            StringBuilder sql = new StringBuilder();

            int len = columns.Count();
            foreach (string[] column in columns)
            {
                --len;

                //if (!column[0].ToUpper().Equals("DTREGISTO")
                //    && !column[0].ToUpper().Equals("CODUTILIZADORREGISTO")
                //    && !column[0].ToUpper().Equals("CODUTILIZADORALTERACAO"))
                if (pks.Contains(column[0]))
                {
                    sql.Append(SqlUtils.transfProcInArg(column[0], column[1], column[2], column[3], column[4]));
                    if (len >= 0)
                    {
                        sql.Append(",");
                    }
                    sql.Append("\n");
                    //if (pks.Contains(column[0]))
                    //{
                    //    sql.Append(SqlUtils.transfProcInArg("old" + column[0], column[1], column[2], column[3], column[4]));
                    //    if (len >= 0)
                    //    {
                    //        sql.Append(",");
                    //    }
                    //    sql.Append("\n");
                    //}
                }

            }
            len = columns.Count();
            foreach (string[] column in columns)
            {
                --len;

                //if (!column[0].ToUpper().Equals("DTREGISTO")
                //    && !column[0].ToUpper().Equals("CODUTILIZADORREGISTO")
                //    && !column[0].ToUpper().Equals("CODUTILIZADORALTERACAO"))
                if (replacePks.Contains(column[0]) && !pks.Contains(column[0]))
                {
                    sql.Append(SqlUtils.transfProcInArg(column[0], column[1], column[2], column[3], column[4]));
                    if (len >= 0)
                    {
                        sql.Append(",");
                    }
                    sql.Append("\n");
                    //if (pks.Contains(column[0]))
                    //{
                    //    sql.Append(SqlUtils.transfProcInArg("old" + column[0], column[1], column[2], column[3], column[4]));
                    //    if (len >= 0)
                    //    {
                    //        sql.Append(",");
                    //    }
                    //    sql.Append("\n");
                    //}
                }

            }

            
            
            
            return sql.ToString();
        }



        public static string generateInsertSQL(List<string[]> columns, string table)
        {
            StringBuilder sql = new StringBuilder();
                
            sql.Append("INSERT INTO ");
            sql.Append(table);
            sql.Append("(\n");

            int len = columns.Count();
            foreach (string[] column in columns)
            {
                --len;
                sql.Append("\t\t\t");
                sql.Append(column[0]);
                if (len > 0)
                {
                    sql.Append(",");
                }
                sql.Append("\n");
                //if (update && replaceablePKs.Contains(column[0]))
                //{
                //    sql.Append(SqlUtils.transfProcInArg("old" + column[0], column[1], column[2], column[3], column[4]));
                //    if (len >= 0)
                //    {
                //        sql.Append(",");
                //    }
                //    sql.Append("\n");
                //}
            }
            sql.Append("\t\t) VALUES (\n");
            
            len = columns.Count();
            foreach (string[] column in columns)
            {
                --len;
                if (column[0].ToUpper().Equals("DTREGISTO"))
                {
                    sql.Append("\t\t\t@dtRegisto");
                }
                else if (column[0].ToUpper().Equals("CODUTILIZADORREGISTO") || column[0].ToUpper().Equals("CODUTILIZADORALTERACAO"))
                {
                    sql.Append("\t\t\t@codUtilizador");
                }
                else
                {
                    sql.Append("\t\t\t@");
                    sql.Append(column[0]);
                }
                if (len > 0)
                {
                    sql.Append(",");
                }
                sql.Append("\n");
                //if (update && replaceablePKs.Contains(column[0]))
                //{
                //    sql.Append(SqlUtils.transfProcInArg("old" + column[0], column[1], column[2], column[3], column[4]));
                //    if (len >= 0)
                //    {
                //        sql.Append(",");
                //    }
                //    sql.Append("\n");
                //}

            }
            sql.Append("\t\t)\n");

            return sql.ToString();
        }


        public static string generateUpdateSQL(List<string[]> columns, string table, List<String> replacePKs, List<string> pks) {
            StringBuilder sql = new StringBuilder();


            //UPDATE [ISP_BD_GAA].[dbo].[COM_BANCO]
            //   SET [CodBanco_PK] = <CodBanco_PK, smallint,>
            //      ,[NomBanco] = <NomBanco, varchar(150),>
            //      ,[FlgActivo] = <FlgActivo, char(1),>
            // WHERE <Search Conditions,,>

            sql.Append("UPDATE ");
            sql.Append(table);
            sql.Append("\n\t\t\tSET \n");
            //sql.Append("(\n");

            int len = 0;
            foreach (string[] column in columns) {

                if (column[0].ToUpper().Equals("CODUTILIZADORALTERACAO")) {
                    if (len != 0) {
                        sql.Append(",\r\n");
                    }
                    len++;

                    sql.Append("\t\t\t\t");
                    sql.Append(column[0]);
                    sql.Append(" = ");
                    sql.Append("@codUtilizador");

                } else if (!column[0].ToUpper().Equals("DTREGISTO") && !column[0].ToUpper().Equals("CODUTILIZADORREGISTO")) {
                    if (len != 0) {
                        sql.Append(",\n");
                    }
                    len++;

                    sql.Append("\t\t\t\t");
                    sql.Append(column[0]);
                    sql.Append(" = ");
                    sql.Append("@");
                    sql.Append(column[0]);

                }
            }
            sql.Append("\r\n\t\t\tWHERE \n");

            len = pks.Count();
            foreach (string column in pks) {
                --len;
                sql.Append("\t\t\t\t");
                sql.Append(column);
                sql.Append(" = ");
                if (replacePKs.Contains(column)) {
                    sql.Append("@old" + column);
                } else {
                    sql.Append("@" + column);
                }
                if (len > 0) {
                    sql.Append(" AND ");
                }
                sql.Append("\n");
            }
            return sql.ToString();
        }

        public static string generateInsertHT(List<string[]> columns, string table, /*List<string> replaceablePKs, */
            List<string> pks, List<string[]> columnsForSelect, string tableForSelect, bool versao)
        {

            StringBuilder sql = new StringBuilder();
            StringBuilder sqlSelect = new StringBuilder();

            sql.Append("INSERT INTO ");
            sql.Append(table);
            sql.Append("\n\t\t(\n");

            int len = columns.Count();
            foreach (string[] column in columns)
            {
                --len;
                sql.Append("\t\t\t");
                sql.Append(column[0]);
                if (len > 0)
                {
                    sql.Append(",");
                }
                sql.Append("\n");

                if (!column[0].ToUpper().Equals("DTREGISTOHT_PK")
                    && !column[0].ToUpper().Equals("CODUTILIZADORALTERACAO")
                    && !column[0].ToUpper().Equals("IDVERSAO")
                    && !column[0].ToUpper().Equals("CODOPERACAO"))
                {
                    sqlSelect.Append("\t\t\t");
                    sqlSelect.Append(column[0]);
                    
                    if (len > 0)
                    {
                        sqlSelect.Append(",");
                    }
                    sqlSelect.Append("\n");

                }
                else if (column[0].ToUpper().Equals("DTREGISTOHT_PK"))
                {
                    sqlSelect.Append("\t\t\t@dtRegisto");
                    if (len > 0)
                    {
                        sqlSelect.Append(",");
                    }
                    sqlSelect.Append("\n");
                }
                else if (column[0].ToUpper().Equals("CODUTILIZADORALTERACAO"))
                {
                    sqlSelect.Append("\t\t\t@codUtilizador");
                    if (len > 0)
                    {
                        sqlSelect.Append(",");
                    }
                    sqlSelect.Append("\n");
                }
                else if (column[0].ToUpper().Equals("IDVERSAO"))
                {
                    sqlSelect.Append("\t\t\t@idVersao");
                    if (len > 0)
                    {
                        sqlSelect.Append(",");
                    }
                    sqlSelect.Append("\n");
                }
                else if (column[0].ToUpper().Equals("CODOPERACAO"))
                {
                    sqlSelect.Append("\t\t\t@CodOperacao");
                    if (len > 0)
                    {
                        sqlSelect.Append(",");
                    }
                    sqlSelect.Append("\n");
                }

                //if (update && replaceablePKs.Contains(column[0]))
                //{
                //    sql.Append(SqlUtils.transfProcInArg("old" + column[0], column[1], column[2], column[3], column[4]));
                //    if (len >= 0)
                //    {
                //        sql.Append(",");
                //    }
                //    sql.Append("\n");
                //}
            }
            sql.Append("\t\t) (\n");
            sql.Append("\t\tSELECT \n");


            sqlSelect.Append("\t\tFROM ");
            sqlSelect.Append(tableForSelect);
            sqlSelect.Append(" \n\t\tWHERE 1=1 ");

            //if (versao) // Usa as PK's especificas parametrizadas no Excel
            //{
            //    foreach (string pk in replaceablePKs)
            //    {
            //        sqlSelect.Append("\n\t\tAND ");
            //        sqlSelect.Append(pk);
            //        sqlSelect.Append(" = @");
            //        sqlSelect.Append(pk);
            //    }
            //}
            //else // Usa todas as PK's
            //{
                foreach (string pk in pks)
                {
                    sqlSelect.Append("\n\t\t\tAND ");
                    sqlSelect.Append(pk);
                    sqlSelect.Append(" = @");
                    sqlSelect.Append(pk);
                }

            //}


            sql.Append(sqlSelect.ToString());
            //len = columns.Count();
            //foreach (string[] column in columns)
            //{
            //    --len;
            //    if (column[0].ToUpper().Equals("DTREGISTO"))
            //    {
            //        sql.Append("\t\t\t@dtRegisto");
            //    }
            //    else if (column[0].ToUpper().Equals("CODUTILIZADORREGISTO") || column[0].ToUpper().Equals("CODUTILIZADORALTERACAO"))
            //    {
            //        sql.Append("\t\t\t@codUtilizador");
            //    }
            //    else
            //    {
            //        sql.Append("\t\t\t@");
            //        sql.Append(column[0]);
            //    }
            //    if (len > 0)
            //    {
            //        sql.Append(",");
            //    }
            //    sql.Append("\n");

            //}
            sql.Append("\n\t\t)\n");

            return sql.ToString();
        }


    }
}
