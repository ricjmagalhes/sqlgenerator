using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.IO;
using System.Reflection;

namespace SqlGenerator {
    class Program {

        static List<Object> parameters = new List<Object>();

        static void Main(string[] args) {
            int i = 0;
            RogerioMain.main();
            if (i == 0)
                return;

            parameters.Add("Gerador");
            parameters.Add(System.DateTime.Now);


            bool update = true;
            bool ht = false;

            if (update)
            {
                generateUpdates(); //UPDATE SP's
            }
            else if (ht)
            {
                generateHT();   //INSERT HT's
            }
            else
            {
                generateInserts(); //INSERT SP's
            }




            //Console.WriteLine(val);
            //Console.ReadLine();
        }

        private static void generateHT()
        {
        //            INSERT INTO [GAA_HIST_BANCO_DEPOSITARIO_FP]
        //(
        //    [NrFundo_PK]
        //   ,[CodNatureza_PK]
        //   ,[CodBanco_PK]
        //   ,[DtInicio_PK]
        //   ,[XObservacoes]
        //   ,[DtFim]
        //   ,[dtRegisto]
        //   ,[CodUtilizadorRegisto]
        //   ,[CodUtilizadorAlteracao]
        //   ,idVersao
        //   ,codOperacao
        //)
        //(
        //SELECT 
        //    [NrFundo_PK]
        //   ,[CodNatureza_PK]
        //   ,[CodBanco_PK]
        //   ,[DtInicio_PK]
        //   ,[XObservacoes]
        //   ,[DtFim]
        //   ,[dtRegisto]
        //   ,[CodUtilizadorRegisto]
        //   ,@codUtilizadorAlteracao
        //    ,@idVersao
        //    ,@codOperacao
        //FROM GAA_BANCO_DEPOSITARIO_FP
        //WHERE [NrFundo_PK] = @nrFundo_PK
        //and [CodNatureza_PK] = @codNatureza_PK
        //)

            try
            {
                Program p = new Program();
                ExcelUtils.initXls();

                //List<Object> parameters = new List<Object>();
                //parameters.Add("Livio Cerqueira");
                //parameters.Add(System.DateTime.Now);

                string template = FileUtils.getTemplateFile("Template_HT.txt");

                List<string> tableList = ExcelUtils.getTableListForProcessing("TABELAS_HT");

                foreach (string tableName in tableList)
                {
                    string table = tableName;
                    string tableForSelect = table.Remove(3, 5);
                    string spName = "SP_GAA_INSERT_HT_" + table.Remove(0, 9);
                    //bool update = true;

                    List<string> pks = SqlUtils.getTablePrimaryKeys(tableForSelect);
                    List<String> replaceablePKs = ExcelUtils.getReplaceablePKs(table);
                    List<String> descs = ExcelUtils.getDescription(table);
                    List<string[]> columns = SqlUtils.getTableColumns(table);
                    List<string[]> columnsForSelect = SqlUtils.getTableColumns(tableForSelect);

                    string inParams = SqlUtils.getInParamsHT(columns, pks, replaceablePKs);

                    //List<String> spVersao = ExcelUtils.getSPVersoes(table);
                    string queryStringVersion = null;
                    string queryStringHT = null;

                    queryStringVersion = SqlUtils.generateInsertHT(columns, table, /*replaceablePKs, */pks, columnsForSelect, tableForSelect, true);
                    queryStringHT = SqlUtils.generateInsertHT(columns, table, /*replaceablePKs, */pks, columnsForSelect, tableForSelect, false);

                    if (descs.Count == 0)
                    {
                        descs.Add("Gerador");
                    }

                    string outFile = string.Format(
                        template,
                        parameters[0].ToString(),   // Nome
                        parameters[1].ToString(),   // Data
                        descs[0].ToString(),        // Desc
                        spName,                     // SP name
                        inParams,                   // In Params
                        queryStringVersion,         // Insert For Version
                        queryStringHT               // Insert for HT
                        );

                    FileUtils.writeGeneratedFile("HT_" + table + ".sql", outFile);
                }
            }
            catch (Exception e)
            {
                //ExcelUtils.closeXLS();
                FileUtils.writeGeneratedFile("EXCEPTION_.txt", e.StackTrace, true);
                //Console.WriteLine(e);
                //Console.ReadLine();
            }
            finally
            {
                ExcelUtils.closeXLS();
            }



        }

        private static void generateUpdates()
        {
            try
            {
                Program p = new Program();
                ExcelUtils.initXls();

                //List<Object> parameters = new List<Object>();
                //parameters.Add("Livio Cerqueira");
                //parameters.Add(System.DateTime.Now);

                string template = FileUtils.getTemplateFile("Template_UPDATE.txt");

                List<string> tableList = ExcelUtils.getTableListForProcessing("TABELAS_GERAR");

                foreach (string tableName in tableList)
                {

                    string table = tableName;
                    string spName = "SP_GAA_UPDATE_" + table.Remove(0,4);
                    bool update = true;



                    List<string> pks = SqlUtils.getTablePrimaryKeys(table);
                    List<String> replaceablePKs = ExcelUtils.getReplaceablePKs(table);
                    List<String> descs = ExcelUtils.getDescription(table);
                    List<string[]> columns = SqlUtils.getTableColumns(table);

                    string inParams = SqlUtils.getInParams(columns, update, replaceablePKs);

                    List<String> spVersao = ExcelUtils.getSPVersoes(table);
                    string queryString = null;

                    queryString = SqlUtils.generateUpdateSQL(columns, table, replaceablePKs, pks);



                    string outFile = string.Format(
                        template,
                        parameters[0].ToString(),
                        parameters[1].ToString(),
                        descs[0].ToString(),
                        spName, 
                        inParams, 
                        spVersao[0],
                        "--TODO", // HT
                        queryString
                        );

                    FileUtils.writeGeneratedFile("UPDATE_" + table + ".sql", outFile);
                }
            }
            catch (Exception e)
            {
                ExcelUtils.closeXLS();
                Console.WriteLine(e);
                Console.ReadLine();
            }
            finally
            {
                ExcelUtils.closeXLS();
            }

        }


        private static void generateInserts()
        {
            try
            {

                //string xx = FormUtils.generateEditUserControlDesigner("xxx", "yyy");
                //xx = string.Format(xx, "1111111111111", "22222222222222222", "3333333333333333333333");
                //FileUtils.writeGeneratedFile("nome_do_ficheiro.extensao", xx);
                ////if (1 == 1) return;
                Program p = new Program();
                ExcelUtils.initXls();

                //string val = p.createHeaderProcedure("GAA_TIPO_PLANO_FPF", "INSERT");





                //List<Object> parameters = new List<Object>();
                //parameters.Add("Livio Cerqueira");
                //parameters.Add(System.DateTime.Now);

                string template = FileUtils.getTemplateFile("Template_INSERT.txt");

                List<string> tableList = ExcelUtils.getTableListForProcessing("TABELAS_GERAR");

                foreach (string tableName in tableList)
                {

                    string table = tableName;
                    string spName = "SP_GAA_INSERT_" + table.Remove(0, 4);
                    bool update = false;



                    List<string> pks = SqlUtils.getTablePrimaryKeys(table);
                    List<String> replaceablePKs = ExcelUtils.getReplaceablePKs(table);
                    List<String> descs = ExcelUtils.getDescription(table);
                    List<string[]> columns = SqlUtils.getTableColumns(table);

                    string inParams = SqlUtils.getInParams(columns, update, replaceablePKs);

                    List<String> spVersao = ExcelUtils.getSPVersoes(table);
                    string queryString = "";
                    queryString = SqlUtils.generateInsertSQL(columns, table);

                    string outFile = string.Format(template, parameters[0].ToString(), parameters[1].ToString(), descs[0].ToString(), spName, inParams, spVersao[0], queryString);

                    FileUtils.writeGeneratedFile("INSERT_" + table + ".sql", outFile);
                }
            }
            catch (Exception e)
            {
                FileUtils.writeGeneratedFile("EXCEPTION_.txt", e.StackTrace, true);
                //ExcelUtils.closeXLS();
                //Console.WriteLine(e);
                //Console.ReadLine();
            }
            finally
            {
                ExcelUtils.closeXLS();
            }

        }



        /*
             ALTER PROCEDURE [dbo].[SP_GAA_UPDATE_PLANO_FUNDO_FECHADO]
		        @nrFundo_PK smallint,
		        @codTipoPlano_PK char(2),
             ALTER PROCEDURE [dbo].[SP_GAA_INSERT_PLANO_FUNDO_FECHADO]
		        @nrFundo_PK smallint,
         * */

        private string createHeaderProcedure(string table, string operation) {
            StringBuilder sql = new StringBuilder();
            sql.Append("CREATE PROCEDURE SP_GAA_");
            sql.Append(operation.ToUpper());
            sql.Append("_");
            sql.Append(table.ToUpper());
            sql.Append("\n");

            List<string> pks = SqlUtils.getTablePrimaryKeys(table);
            List<String> replaceablePKs = ExcelUtils.getReplaceablePKs(table);
            List<String> descs = ExcelUtils.getDescription(table);
            List<string[]> columns = SqlUtils.getTableColumns(table);

            int len = columns.Count();
            foreach(string[] column in columns) {
                --len;
                sql.Append(SqlUtils.transfProcInArg(column[0], column[1], column[2], column[3], column[4]));
                if (len > 0) {
                    sql.Append(",");
                }
                sql.Append("\n");
                if (replaceablePKs.Contains(column[0])) {
                    sql.Append(SqlUtils.transfProcInArg("old" + column[0], column[1], column[2], column[3], column[4]));
                    if (len > 0) {
                        sql.Append(",");
                    }
                    sql.Append("\n");
                }
            }
            return sql.ToString();
        }
        
        
        

        


    }
}
