using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace SqlGenerator {
    class ExcelUtils {

        private static Microsoft.Office.Interop.Excel.Workbook workbook = null;
        private static Microsoft.Office.Interop.Excel.Application xlsApp = null;

        public static void initXls() {
            xlsApp = new Microsoft.Office.Interop.Excel.ApplicationClass();
            //workbook = xlsApp.Workbooks.Open("C:/Users/lmvcerqueira/Desktop/SQL_GEN.xlsx");
            workbook = xlsApp.Workbooks.Open(Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"templates\SQL_GEN.xlsx")); 
            //workbook = xlsApp.Workbooks.Open("C:checkUserscheckcapgeminidev2/Desktop/SQL_GEN.xlsx");
        }

        public static void closeXLS()
        {
            //xlsApp = new Microsoft.Office.Interop.Excel.ApplicationClass();
            if (workbook != null) {
                workbook.Close();
            }
        }

        public static List<string> getTableListForProcessing(string rangeName)
        {
            List<string> values = new List<string>();
            Microsoft.Office.Interop.Excel.Range range = getNamedRange(rangeName);
            if (range != null)
            {
                foreach (Microsoft.Office.Interop.Excel.Range row in range.Rows)
                {
                    if (((Microsoft.Office.Interop.Excel.Range)row.Cells["1", "A"]).Value2 != null)
                    {
                        values.Add(((Microsoft.Office.Interop.Excel.Range)row.Cells["1", "A"]).Value2.ToString());
                    }
                }
            }
            return values;
        }



        public static List<string> getReplaceablePKs(string table) {
            List<string> values = new List<string>();
            Microsoft.Office.Interop.Excel.Range range = getNamedRange(table);
            if (range != null) {
                foreach (Microsoft.Office.Interop.Excel.Range row in range.Rows) {
                    if (((Microsoft.Office.Interop.Excel.Range)row.Cells["1", "A"]).Value2 != null) {
                        values.Add(((Microsoft.Office.Interop.Excel.Range)row.Cells["1", "A"]).Value2.ToString());
                    }
                }
            }
            return values;
        }

        public static Microsoft.Office.Interop.Excel.Range getNamedRange(string rangeName) {
            foreach (Microsoft.Office.Interop.Excel.Name name in workbook.Names) {
                if (name.Name.Equals(rangeName))
                    return name.RefersToRange;
            }
            return null;
        }


        public static List<string> getDescription(string table)
        {
            List<string> values = new List<string>();
            Microsoft.Office.Interop.Excel.Range range = getNamedRange(table);
            if (range != null)
            {
                foreach (Microsoft.Office.Interop.Excel.Range row in range.Rows)
                {
                    if (((Microsoft.Office.Interop.Excel.Range)row.Cells["1", "B"]).Value2 != null)
                    {
                        values.Add(((Microsoft.Office.Interop.Excel.Range)row.Cells["1", "B"]).Value2.ToString());
                    }
                }
            }
            return values;
        }

        public static List<string> getSPVersoes(string table)
        {
            List<string> values = new List<string>();
            Microsoft.Office.Interop.Excel.Range range = getNamedRange(table);
            if (range != null)
            {
                foreach (Microsoft.Office.Interop.Excel.Range row in range.Rows)
                {
                    if (((Microsoft.Office.Interop.Excel.Range)row.Cells["1", "C"]).Value2 != null)
                    {
                        values.Add(((Microsoft.Office.Interop.Excel.Range)row.Cells["1", "C"]).Value2.ToString());
                    }
                }
            }
            return values;
        }



        public static string getColumnDescription(string key) {
            
            Microsoft.Office.Interop.Excel.Range range = getNamedRange("Dictionary");
            if (range != null) {
                foreach (Microsoft.Office.Interop.Excel.Range row in range.Rows) {
                    return ((Microsoft.Office.Interop.Excel.Range)row.Cells["1", "A"]).Value2.ToString();
                }
            }
            return key;
        }

    }
}
