using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace SqlGenerator {
    class FormUtils {

        //public static string generateSPFromTemplate(string templateName) {
        //    return FileUtils.getTemplateFile(templateName);
        //}

        public static void generateEditFormUI(string entity, List<string[]> columns, List<string> pks, List<string> replaceablePKs) {
            string hiddens = "";
            string fields = "";
            foreach (string s in pks) {
                hiddens += string.Format("<asp:HiddenField ID=\"h_" + s + "\" runat=\"server\" Visible=\"False\" Value=\"\" />\r\n");
            }

            foreach (string s in replaceablePKs) {
                hiddens += string.Format("<asp:HiddenField ID=\"txt_old" + s + "\" runat=\"server\" Visible=\"False\" Value=\"\" />\r\n");
            }
            int validator = 0;
            foreach (string[] column in columns) {
                bool required = "0".Equals(columns[5]);
                string reqStr = "\t\t\t<asp:RequiredFieldValidator ID=\"validatorNomeCampo" + (validator++.ToString()) + "\" " +
                    "runat=\"server\" ControlToValidate=\"txt_" + column[0] + "\" " +
                    "ErrorMessage=\"Coloque XXXXXX\" ValidationGroup=\"" + entity + "\">*</asp:RequiredFieldValidator>";
                string field = "";
                //if ("bit".Equals(column[1]) || isCheckBox(column)) {
                //    field = FileUtils.getTemplateFile("InputCheckBox.txt");
                //    field = string.Format(field, column[0]);
                //} else {
                //    if (column[0].EndsWith("FK")) {
                //        field = FileUtils.getTemplateFile("InputDropDown.txt");
                //        field = string.Format(field, column[0]);
                //    } else {
                //        field = FileUtils.getTemplateFile("InputText.txt");
                //        field = string.Format(field, column[0], column[0], "false", "true", reqStr);
                //    }
                //}
                switch (column[1]) {
                    case "datetime":
                    case "smalldatetime":
                        field = FileUtils.getTemplateFile("InputDate.txt");
                        field = string.Format(field, column[0], column[0], validator++.ToString(), entity);
                        break;
                    case "bit":
                    case "smallint":
                    case "int":
                    case "bigint":
                    case "char":
                    case "decimal":
                    case "varchar":
                        if ("bit".Equals(column[1]) || isCheckBox(column)) {
                            field = FileUtils.getTemplateFile("InputCheckBox.txt");
                            field = string.Format(field, column[0]);
                        } else {
                            if (column[0].EndsWith("FK")) {
                                field = FileUtils.getTemplateFile("InputDropDown.txt");
                                field = string.Format(field, column[0]);
                            } else {
                                field = FileUtils.getTemplateFile("InputText.txt");
                                field = string.Format(field, column[0], column[0], "false", "true", reqStr);
                            }
                        }
                        break;
                    default:
                        field = "\n\n\n--------------" + column[0] + "------------------\n\n\n";
                        break;
                }
                fields += field + "\r\n";
            }
            string html = FileUtils.getTemplateFile("UserControlEditUI.txt");
            html = string.Format(html, entity, hiddens, fields);
            FileUtils.writeGeneratedFile("UC_Editar" + entity + ".aspx", html);
        }

        public static void generateEditFormCode(string t1, string t2, string entity, 
            List<string[]> columnsT1, List<string[]> columnsT2, 
            List<string> pksT1, List<string> pksT2,
            List<String> replaceablePKsT1, List<String> replaceablePKsT2) {


            string spSufix1 = t1.Substring(4);
            List<string[]> columns = DbUtils.unionColumns(columnsT1, columnsT2);
            string html = null;
            string reset = resetFields(columns);

            string insertT1 = gravarAction(columnsT1, pksT1, replaceablePKsT1, spSufix1, false);
            string updateT1 = gravarAction(columnsT1, pksT1, replaceablePKsT1, spSufix1, true);
            string settersUpdateT1 = setterModel(columnsT1, pksT1, true);
            string settersInsertT1 = parseInputParameters(columnsT1, pksT1);  
            string detailT1 = doSelect(columnsT1, pksT1);
            string ddlEditT1 = generateDdl(columnsT1, true);
            string ddlNewT1 = generateDdl(columnsT1, false);
            string query1 = "SP_GAA_DETALHE_" + spSufix1 + " (\r\n" +
                parseSpInputParameters(columnsT1, pksT1);
            query1 = query1.Substring(0, query1.Length - 3) + ")";


            if (t2 == null) {
                html = FileUtils.getTemplateFile("UserControlCode1.txt");
                html = string.Format(html,
                    "UC_Editar" + entity,
                    reset,
                    query1,
                    settersUpdateT1 + ddlEditT1,
                    settersInsertT1 + ddlNewT1,
                    updateT1,
                    insertT1);
            } else {
                FileUtils.getTemplateFile("UserControlCode.txt");
                string spSufix2 = t2.Substring(4);
                string insertT2 = gravarAction(columnsT2, pksT2, replaceablePKsT2, spSufix2, false);
                string updateT2 = gravarAction(columnsT2, pksT2, replaceablePKsT2, spSufix2, true);
                string settersUpdateT2 = setterModel(columnsT2, pksT2, true);
                string settersInsertT2 = parseInputParameters(columnsT2, pksT2);
                string detailT2 = doSelect(columnsT2, pksT2);
                string ddlEditT2 = generateDdl(columnsT2, true);
                string ddlNewT2 = generateDdl(columnsT2, false);
                string query2 = "";
                if (t2 != null) {
                    query2 = "SP_GAA_DETALHE_" + spSufix2 + " (\r\n" +
                        parseSpInputParameters(columnsT2, pksT2);
                    query2 = query2.Substring(0, query2.Length - 3) + ")";
                }
                html = string.Format(html,
                    "UC_Editar" + entity,
                    reset,
                    query1,
                    settersUpdateT1 + ddlEditT1,
                    query2,
                    settersUpdateT2 + ddlEditT2,
                    settersInsertT1 + ddlNewT1,
                    settersInsertT2 + ddlNewT2,
                    updateT1,
                    updateT2,
                    insertT1,
                    insertT2);
            }
            FileUtils.writeGeneratedFile("UC_Editar" + entity + ".ascx.cs", html);
        }

        private static string resetFields(List<string[]> columns) {
            string value = "\t\t\tthis.versionUC = versionUC;\r\n" +
                "\t\t\th_Localizacao.Value = localizacao.ToString();\r\n";
            foreach (string[] column in columns) {
                string field = null;
                if (isCheckBox(column)) {
                    field = "\t\t\tchk_" + column[0] + ".Checked = false;\r\n";
                } else if (!column[0].EndsWith("FK")) {
                    field = "\t\t\ttxt_" + column[0] + ".Text = null;\r\n";
                }
                value += field;
            }
            return value;
        }
        private static string parseInputParameters(List<string[]> columns, List<string> pks) {
            string value = "";
            int index = 1;
            foreach (string[] column in columns) {
                if (pks.Contains(column[0])) {
                    value += "\t\t\t\t\th_" + column[0] + ".Value = arguments[" + index.ToString() + "] == null ? \"\" : arguments[" + index++.ToString() + "].ToString();\r\n";
                }
            }
            return value;
        }
        private static string parseSpInputParameters(List<string[]> columns, List<string> pks) {
            string value = "";
            int index = 1;
            foreach (string[] column in columns) {
                if (pks.Contains(column[0])) {
                    value += "\t\t\t\t\t\t" + convertFunctionArgument(column[1], index++) + ",\r\n";
                }
            }
            return value;
        }
        private static string convertFunctionArgument(string column, int index) {
            string field = "";
            if ("smallint".Equals(column)) {
                field = "Convert.ToInt16(arguments[" + index.ToString() + "])";
            } else if ("int".Equals(column)) {
                field = "Convert.ToInt32(arguments[" + index.ToString() + "])";
            } else if ("bigint".Equals(column)) {
                field = "Convert.ToInt64(arguments[" + index.ToString() + "])";
            } else if ("datetime".Equals(column)) {
                field = "(DateTime)(arguments[" + index.ToString() + "])";
            } else {
                field = "(string)arguments[" + index.ToString() + "]";
            }
            return field;
        }
        private static string gravarAction(List<string[]> columns, List<string> pks, List<string> replaceablePKs, string spSufix, bool update) {
            string value = "\t\t\t\t\tstring natureza = Localizacao.FUNDO_FECHADO.ToString().Equals(h_Localizacao.Value) ? GlobalVar.NATUREZA_FUNDO_FECHADO.ToString() : GlobalVar.NATUREZA_FUNDO_ABERTO.ToString();\r\n";
            if (update) {
                value += "\t\t\t\t\tgetDb().SP_GAA_UPDATE_" + spSufix + " (\r\n";
                foreach (string[] column in columns) {
                    value += "\t\t\t\t\t\t" + spInputArg(column, "", null) + ",\r\n";
                    if (replaceablePKs.Contains(column[0])) {
                        value += "\t\t\t\t\t\t" + spInputArg(column, "old", ".Value") + ",\r\n";
                    }
                }
                value += "\t\t\t\t\t\t" + "((AbstractPage)Page).getCodUtilizador()" + ",\r\n";
                value += "\t\t\t\t\t\t\"" + "ALTERACAO" + "\",\r\n";
            } else {
                value += "\t\t\t\t\t\tgetDb().SP_GAA_INSERT_" + spSufix + " (\r\n";
                foreach (string[] column in columns) {
                    value += "\t\t\t\t\t\t" + spInputArg(column, "", null) + ",\r\n";
                }
                value += "\t\t\t\t\t\t" + "((AbstractPage)Page).getCodUtilizador()" + ",\r\n";
                value += "\t\t\t\t\t\t\"" + "REGISTO" + "\",\r\n";
            }
            value += "\t\t\t\t\t\t" + "flgAlteracao" + ",\r\n";
            value += "\t\t\t\t\t\t" + "flgIncrementaVersao" + "\r\n";
            value += "\t\t\t\t\t);\r\n";
            return value;
        }

        private static bool isCheckBox(string[] column) {
            return "char".Equals(column[1]) && "1".Equals(column[2]);
        }
        
        private static string spInputArg(string[] column, string prefix, string attr) {
            string field = "";
            if ("smallint".Equals(column[1])) {
                field = "Convert.ToInt16(" + "txt_" + prefix + column[0] + (attr == null ? ".Text" : attr) + ")";
            } else if ("int".Equals(column[1])) {
                field = "Convert.ToInt32(" + "txt_" + prefix + column[0] + (attr == null ? ".Text" : attr) + ")";
            } else if ("bigint".Equals(column[1])) {
                field = "Convert.ToInt64(" + "txt_" + prefix + column[0] + (attr == null ? ".Text" : attr) + ")";
            } else if ("decimal".Equals(column[1])) {
                field = "Convert.ToDecimal(" + "txt_" + prefix + column[0] + (attr == null ? ".Text" : attr) + ")";
            } else if ("datetime".Equals(column[1])) {
                field = "Utils.convertToDate(" + "txt_" + prefix + column[0] + (attr == null ? ".Text" : attr) + ")";
            } else if ("bit".Equals(column[1]) || isCheckBox(column)) {
                field = "chk_" + prefix + column[0] + ".Checked ? \"V\" : \"F\"";
            } else {
                if (column[0].EndsWith("FK")) {
                    field = "ddl_" + column[0] + (attr == null ? ".SelectedValue" : attr);
                } else {
                    field = "txt_" + prefix + column[0] + (attr == null ? ".Text" : attr);
                }
            }
            return field;
        }

        private static string setterModel(List<string[]> columns, List<string> pks, bool update = false) {
            string value = "";
            //int arg = 1;
            foreach (string[] column in columns) {
                string field = "";
                if (update) {
                    if (column[0].EndsWith("PK")) {
                        field = "\t\t\t\t\th_" + column[0] + ".Value = query." + column[0] + ".ToString();\r\n";
                    } else if (update) {
                        if ("datetime".Equals(column[1])) {
                            field = "\t\t\t\t\ttxt_" + column[0] + ".Text = Utils.formatDate(query." + column[0] + ");\r\n";
                        } else if ("bit".Equals(column[1]) || ("char".Equals(column[1]) && "1".Equals(column[2]))) {
                            field = "\t\t\t\t\tchk_" + column[0] + ".Checked = query." + column[0] + ".Equals(\"V\");\r\n";
                        } else {
                            if (column[0].EndsWith("FK")) {
                                field = "\t\t\t\t\tUtils.load_ddl(ddl_" + column[0] + ", true, query." + column[0] + ");\r\n";
                            } else {
                                field = "\t\t\t\t\ttxt_" + column[0] + ".Text = query." + column[0] + ".ToString();\r\n";
                            }
                        }
                    }
                } else {
                    if (column[0].EndsWith("PK")) {
                        field = "\t\t\t\t\th_" + column[0] + ".Value = ";
                        string cast = "(string)";
                        if (column[1].Contains("char")) {
                            cast += "txt_" + column[0] + ".Text";
                        } else if (column[1].Contains("datetime")) {
                            cast = "(DateTime)" + column[0];
                        } else if (column[1].Contains("smallint")) {
                            cast = "Convert.ToInt16(txt_" + column[0] + ".ToString())";
                        } else if (column[1].Contains("bigint")) {
                            cast = "Convert.ToInt64(txt_" + column[0] + ".ToString())";
                        } else if (column[1].Contains("int")) {
                            cast = "Convert.ToInt32(txt_" + column[0] + ".ToString())";
                        } else {
                            cast = "Convert.ToInt??_" + columns[1] + "(txt_" + column[0] + ".ToString())";
                        }
                        field += cast + ";\r\n";
                    //} else if (update) {
                    //    if ("datetime".Equals(column[1])) {
                    //        field = "\t\t\t\t\ttxt_" + column[0] + ".Text = Utils.formatDate(query." + column[0] + ");\r\n";
                    //    } else if ("bit".Equals(column[1]) || ("char".Equals(column[1]) && "1".Equals(column[2]))) {
                    //        field = "\t\t\t\t\tchk_" + column[0] + ".Checked = query." + column[0] + ".Equals(\"V\");\r\n";
                    //    } else {
                    //        if (column[0].EndsWith("FK")) {
                    //            field = "\t\t\t\t\tUtils.load_ddl(ddl_" + column[0] + ", true, query." + column[0] + ");\r\n";
                    //        } else {
                    //            field = "\t\t\t\t\ttxt_" + column[0] + ".Text = query." + column[0] + ".ToString();\r\n";
                    //        }
                    //    }
                    }
                }
                value += field;
            }
            return value;
        }

        private static string doSelect(List<string[]> columns, List<string> pks) {
            string value = "\t\t\t\t\tvar query = (from r in getDb().SP_GAA_XXXXXXXX(\r\n";
            int index = 1;
            foreach (string[] column in columns) {
                if (pks.Contains(column[0])) {
                    if (column[1].Contains("char")) {
                        value += "\t\t\t\t\t\t(string)arguments[" + index + "],\r\n";
                    } else if (!"datetime".Equals(column[1])) {
                        value += "\t\t\t\t\t\tConvert.ToInt16(arguments[" + index + "]),\r\n";
                    } else {
                        value += "\t\t\t\t\t\t(DateTime)arguments[" + index + "],\r\n";
                    }
                    index++;
                }
            }
            return value + "\t\t\t\t\t)select r).First();\r\n";
        }

        private static string generateDdl(List<string[]> columns, bool edit) {
            string value = "\r\n";
            foreach (string[] column in columns) {
                if (column[0].EndsWith("FK")) {
                    value += "\t\t\t\t\tUtils.load" + column[0] + "DDL(ddl_" + column[0] + ", true";
                    if (edit) {
                        value += ", query." + column[0];
                    }
                    value += ");\r\n";                    
                }
            }
            return value;
        }
    }
}
