using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Reflection;

namespace SqlGenerator {
    class FileUtils {
        public static string getTemplateFile(string template) {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"templates\" + template);
            return File.ReadAllText(path);
        }

        public static void writeGeneratedFile(string filename, string content) {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"output\" + filename);
            TextWriter writer = new StreamWriter(path, false);
            writer.WriteLine(content);
            writer.Close();
        }
        public static void writeGeneratedFile(string filename, string content, bool append)
        {
            string path = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"output\" + filename);
            TextWriter writer = new StreamWriter(path, append);
            writer.WriteLine(content);
            writer.Close();
        }
    }
}
