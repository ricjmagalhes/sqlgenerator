﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SqlGenerator {
    class DbUtils {
        
        private static string getAuthorName() {
             return "Generator";
        }
        private static string getCreateDate() {
             return Utils.formatDate(DateTime.Now);
        }

        
        //List<String> descs = ExcelUtils.getDescription(tableName);
        //List<String> spVersao = ExcelUtils.getSPVersoes(tableName);
        //List<String> replaceablePKs = ExcelUtils.getReplaceablePKs(tableName);
        //List<string> pks = SqlUtils.getTablePrimaryKeys(tableName);
        //List<string[]> columns = SqlUtils.getTableColumns(tableName);

        public static string generateInserts(string tableName, string tipoFundo,
            String description, 
            String version,
            List<string[]> columns,
            List<string> pks,
            List<String> replaceablePKs) {

            string template = FileUtils.getTemplateFile("Template_INSERT.txt");
            string spName = "SP_GAA_INSERT_" + tableName.Remove(0, 4);

            string inParams = SqlUtils.getInParams(columns, false, replaceablePKs);
            string queryString = SqlUtils.generateInsertSQL(columns, tableName);

            return string.Format(template,
                getAuthorName(),
                getCreateDate(),
                description,
                spName,
                inParams,
                "EXEC SP_GAA_REGISTA_VERSAO_" + tipoFundo + " @codUtilizador, @CodEntidade_PK, null, @codOperacao, @idVersao OUTPUT, @dtRegisto OUTPUT;",
                queryString);
        }

        public static string generateUpdates(string tableName, 
            String description, 
            String version,
            List<string[]> columns,
            List<string> pks,
            List<String> replaceablePKs) {
            
            string template = FileUtils.getTemplateFile("Template_UPDATE.txt");
            string spName = "SP_GAA_UPDATE_" + tableName.Remove(0, 4);
            string inParams = SqlUtils.getInParams(columns, true, replaceablePKs);
            string queryString = SqlUtils.generateUpdateSQL(columns, tableName, replaceablePKs, pks);

            string args = "";
            foreach (string[] column in columns) {
                if (pks.Contains(column[0])) {
                    args += "\t\t\t\t\t@" + column[0] + ", \r\n";
                }
            }
            string historico = "EXEC SP_GAA_INSERT_HT_" + tableName.Substring(4) + " \r\n" + 
                args +
                "\t\t\t\t\t@codUtilizador, @codOperacao, @dtRegisto OUTPUT, @idVersao, @flgIncrementaVersao; \r\n";

            return string.Format(
                template,
                getAuthorName(),
                getCreateDate(),
                description,
                spName,
                inParams,
                "EXEC SP_GAA_REGISTA_VERSAO_FUNDO_FECHADO @codUtilizador, null, @NrFundo_PK, @codOperacao, @idVersao OUTPUT, @dtRegisto OUTPUT;",
                historico,
                queryString);
        }

        public static string generateHT(string tableName, string histTable,
            String description, 
            String version,
            List<string[]> columns,
            List<string> pks,
            List<String> replaceablePKs) {
            
            string template = FileUtils.getTemplateFile("Template_HT.txt");
            //string tableForSelect = ;
            string spName = "SP_GAA_INSERT_HT_" + tableName.Substring(4);
            List<string[]> columnsForSelect = SqlUtils.getTableColumns(histTable);
            string inParams = SqlUtils.getInParamsHT(columns, pks, replaceablePKs);
            string queryStringVersion = SqlUtils.generateInsertHT(columnsForSelect, histTable, /*replaceablePKs, */pks, columns, tableName, true);
            string queryStringHT = SqlUtils.generateInsertHT(columnsForSelect, histTable, /*replaceablePKs, */pks, columns, tableName, false);

            return string.Format(
                template,
                getAuthorName(),
                getCreateDate(),
                description,                // Desc
                spName,                     // SP name
                inParams,                   // In Params
                queryStringVersion,         // Insert For Version
                queryStringHT               // Insert for HT
                );
        }

        public static string generateDetail(string tableName, 
            String description, 
            String version,
            List<string[]> columns,
            List<string> pks) {

            string template = FileUtils.getTemplateFile("Template_DETAIL.txt");
            string spName = "SP_GAA_DETALHE_" + tableName.Remove(0, 4);
            int size = columns.Count() - 1;
            string inParams = "";
            foreach (string[] column in columns) {
                if (pks.Contains(column[0])) {
                    inParams += "@" + column[0] + " " + getSqlType(column);
                    inParams += " = null";
                    if (--size > 1) inParams += ", ";
                    else inParams += "\r\n";
                }
            }
            //inParams = inParams.Substring(0, inParams.Length - 4
            string query = "\t\tselect \r\n\t\t\t";
            size = columns.Count();
            foreach (string[] column in columns) {
                query += column[0];
                if (--size > 0) query += ", ";
                else query += "\r\n";
            }
            query += "\t\tfrom " + tableName + "\r\n";
            query += "\t\twhere 1 = 1 \r\n";
            size = pks.Count();
            foreach (string column in pks) {
                query += "\t\t\tand " + column + " = ISNULL(@" + column + ", " + column + ")\r\n";
            }
            return string.Format(
                template,
                getAuthorName(),
                getCreateDate(),
                description,
                spName,
                inParams,
                "",
                query);

            
        }

        private static string getSqlType(string[] column) {
            if (column[2] != null) {
                return column[1] + "(" + column[2] + ")";
            } else if (column[3] != null && Convert.ToInt16(column[4]) > 0) {
                return column[1] + "(" + column[3] + ", " + column[4] + ")";
            } else {
                return column[1];
            }
        }


        public static List<string[]> unionColumns(List<string[]> columnsT1, List<string[]> columnsT2) {
            List<string[]> value = new List<string[]>();
            List<string> keys = new List<string>();

            foreach (string[] column in columnsT1) {
                keys.Add(column[0]);
                value.Add(column);
            }

            foreach (string[] column in columnsT2) {
                if (!keys.Contains(column[0])) {
                    value.Add(column);
                }
            }
            return value;
        }

        public static List<string> unionPksColumns(List<string> pksT1, List<string> pksT2) {
            List<string> value = new List<string>();


            foreach (string column in pksT1) {
                value.Add(column);
            }

            foreach (string column in pksT2) {
                if (!value.Contains(column)) {
                    value.Add(column);
                }
            }
            return value;
        }
    }
}
